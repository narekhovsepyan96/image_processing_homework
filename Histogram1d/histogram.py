import numpy as np
import matplotlib.pyplot as plt


class histogram1d:
    def __init__(self,a,bin,xlow,xup):
        self._a = a
        self._bin = bin
        self._xlow = xlow
        self._xup = xup
        self._bin_edges = None
        self._hist = None
        self.BLOCK = 65536

    def _search_sorted_inclusive(self,a, v):
        """
        Like `searchsorted`, but where the last item in `v` is placed on the right.
        In the context of a histogram, this makes the last bin edge inclusive
        """
        return np.concatenate((
            a.searchsorted(v[:-1], 'left'),
            a.searchsorted(v[-1:], 'right')
        ))

    def _calc_centers(self):
        self._centers = (self._bin_edges[:-1] + self._bin_edges[1:])/2

    def _calc_binedges(self):
        type = np.result_type(self._xlow,self._xup,self._a)
        self._bin_edges = np.linspace(self._xlow,self._xup,self._bin+1,endpoint=True,dtype=type)
        self._calc_centers()

    def _calc_hist(self):
        self._calc_binedges()
        cum_n = np.zeros(self._bin_edges.shape, np.intp)
        for i in range(0, len(self._a), self.BLOCK):
            sa = np.sort(self._a[i:i+self.BLOCK])
            cum_n += self._search_sorted_inclusive(sa, self._bin_edges)
        self._hist = np.diff(cum_n)

    def get_bin_centers(self,i):
        return self._centers[i]

    def get_bin_width(self):
        return 0.7 * (self._bin_edges[1] - self._bin_edges[0])

    def draw_graph(self):
        self._calc_hist()
        bin = np.zeros(self._bin)
        width = np.zeros(self._bin)
        for i in range(self._bin):
            bin[i] = self.get_bin_centers(i)
            width[i] = self.get_bin_width()
        plt.bar(bin,self._hist,align='center',width=width)
        plt.show()

    def save_plot(self,fname:str) -> None:
        bins = np.zeros(self._bin)
        width = np.zeros(self._bin)
        for i in range(self._bin):
            bins[i] = self.get_bin_centers(i)
            width[i] = self.get_bin_width()
        plt.bar(bin,self._hist,width)

    def get_bin(self,x):
        assert isinstance(x, object)
        return np.where(self._centers == x)

    def get_mean(self):
        return sum(self._hist)/self._hist.size

    def get_content_bin(self,i):
        return self._bin[i]

    def get_minimum_hist(self):
        return min(self._hist)



if __name__ == "__main__":
    mu, sigma = 100, 15
    x = mu + sigma * np.random.randn(10000)
    hi = histogram1d(x,100,min(x),max(x))
    hi.draw_graph()
    print(hi.get_mean())
