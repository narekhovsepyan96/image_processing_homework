import numpy as np
import cv2


img = cv2.imread('moo2.jpg',0)

img_array = np.asarray(img)

hist = np.bincount(img_array.flatten(),minlength=256)

num_pixels = np.sum(hist)
hist = hist / num_pixels

chist = np.cumsum(hist)

_table = np.floor(255 * chist).astype(np.uint8)

img_list = list(img_array.flatten())

eq_img = [_table[p] for p in img_list]

re_eq_img = np.reshape(np.asarray(eq_img),img_array.shape)

cv2.imshow('Source',img)
cv2.imshow('Equalization',re_eq_img)
cv2.waitKey(0)
cv2.destroyAllWindows()




