import numpy as np
import cv2

src = cv2.imread('moo2.jpg',0)

dst = cv2.equalizeHist(src)

cv2.imshow('Source',src)
cv2.imshow('Equalized',dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
