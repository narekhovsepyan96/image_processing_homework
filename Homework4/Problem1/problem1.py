"""
Implement gamma correction, apply different gamma's and explain
"""
import numpy as np
import cv2 as cv


def gamm_correction(img,gamma):
    table = np.empty((1,256),np.uint8)
    for i in range(256):
        table[0,i] = np.clip(pow(i/255.0,gamma)*255.0,0,255)
    res = cv.LUT(img,table)
    return res

img = cv.imread('horse.png')


_list = [1.0,1.8,2.2,2.8,4.0]

_new_img = [gamm_correction(img,i) for i in _list]

im_h = cv.hconcat(_new_img)



cv.imshow('New Images',im_h)
cv.waitKey(0)
cv.destroyAllWindows()
