"""
`Գրել ֆունկցիա, որը ստանում է 2 վեկտոր և վերադարձնում է նրանց սկալյար արտադրյալը
"""

if __name__ == "__main__":
    u_v = [1,2,3,1]
    v_v = [0,1,0,3]
    sum_d: int = 0
    for i_v,j_v in zip(u_v,v_v):
        sum_d += (i_v * j_v)
    print(f"Scalar -> {sum_d}")
