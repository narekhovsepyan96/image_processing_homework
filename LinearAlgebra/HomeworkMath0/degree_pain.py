"""Գրել ֆունցկիա, որը կվերադարձնի տարածության մեջ նշված 2 հարթությունների կազմած անկյունը"""

import math


def degre_2plane(v_p1,v_p2):
    """dot product"""
    v_sum = 0
    for i_d, j_d in zip(v_p1,v_p2):
        v_sum += (i_d * j_d)
    n_1 = 0
    for i_n in v_p1:
        n_1 += i_n**2
    n_1 = math.sqrt(n_1)
    n_2 = 0
    for j_n in v_p2:
        n_2 += j_n**2
    n_2 = math.sqrt(n_2)
    v_sum = v_sum /(n_1 * n_2)
    return math.degrees(math.acos(v_sum))

if __name__ == "__main__":
    p_1 = [1,1,2]
    p_2 = [2,-1,1]
    print(degre_2plane(p_1,p_2))
