"""Ունենք A վեկտոր, ինչ կստացվի այն (կոորդինատները) å անկյունով պտտելու արդյունքում"""

import math

def degre_rotate(in_v, deg_v):
    """ Mult vector sin and cos"""
    x_1 = math.cos(deg_v) * in_v[0] + math.sin(deg_v) * in_v[1]
    y_1 = math.sin(deg_v) * in_v[0] + math.cos(deg_v) * in_v[1]
    return [x_1,y_1]

if __name__ == "__main__":
    v_t = [8,2]
    d_v:int = 30
    print(degre_rotate(v_t,d_v))
