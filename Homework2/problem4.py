"""From standard input read integer numbers until -1 is met.
...After that remove the most common elements and display on standard output the
...result and the common value occurence"""

sum_list = []

while True:
    tmp = int(input())
    if tmp == -1:
        break
    sum_list.append(tmp)


print("The list is: " + str(sum_list))


result = [i for n, i in enumerate(sum_list) if i not in sum_list[:n]]

print(f"The list after removing duplicates: {result}")
