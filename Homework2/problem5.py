"""Read from a file a list of integers, read from command line a range
...and remove the specified range from the list."""

file_name = input("input file name-> ")

with open(file_name,'r',encoding = 'utf-8') as file:
    list_ = []
    for i in file.read().split(','):
        list_.append(int(i))


begin = int(input("begin-> "))
end = int(input("end-> "))
end = end +1


del list_[begin:end]

print(list_)
