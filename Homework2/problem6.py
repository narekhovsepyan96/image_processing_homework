"""Define a phonebook name, phone number, read from command line
...a name and if the name is found print out the phone number."""

phonebook = {"Narek" : "098260704", "Gevor" : "098120305"}

pp = input()

if pp.title() in phonebook:
    print(phonebook[pp.title()])
