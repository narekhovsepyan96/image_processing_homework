"""Find in file the given string with and substitute
...with the other one that is provided from command line."""

with open("input.txt", 'r+', encoding='utf-8') as file_:
    new_string = input("For finding: ")
    text = file_.read()
    rep_string = input("For substitude: ")
    if new_string in text:
        text.replace(new_string, rep_string)
        file_.write(text)
