"""Problem 1. Read from a csv file car parking database,
add support to filter by car model, issue-date."""

import pandas as pd

date = pd.read_csv("car.csv")

print(date.filter({"car model","issue-date"}))
