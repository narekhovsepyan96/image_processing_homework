"""Count the vowels in a string."""

def countv(string_: str):
    """Counting the vowels"""
    count =0
    vowel = set("aeiouAEIOU")
    for i in string_:
        if i in vowel:
            count = count + 1
    return count
