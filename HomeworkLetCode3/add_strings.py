"""Given two non-negative integers, num1 and num2 represented as string,
    return the sum of num1 and num2 as a string.
    You must solve the problem without using any built-in library for
    handling large integers (such as BigInteger).
    You must also not convert the inputs to integers directly.
"""
def add_strings(num_1: str, num_2: str) -> str:
    """Sum Two Big Number"""
    if len(num_1) < len(num_2):
        num_1,num_2 = num_2,num_1

    int_sum: str =""
    i_carry: int = 0
    i_num: int = 0
    i_len = len(num_1) - len(num_2)
    for _ in range(i_len):
        num_2 = "0" + num_2

    num_1 = num_1[::-1]
    num_2 = num_2[::-1]
    i_len = len(num_1)
    for i_ct in range(i_len):
        i_num = (ord(num_1[i_ct])-48)
        i_num += (ord(num_2[i_ct])-48)
        i_num += i_carry
        i_carry = i_num // 10
        if len(num_1) - i_ct != 1:
            i_num %= 10
        int_sum = str(i_num) + int_sum

    return int_sum

if __name__ == "__main__":
    list_n = [["9","1"],
          ["999","999"],
          ["98","9"]]

    for i in list_n:
        print(f"test {i} -> {add_strings(i[0],i[1])}")
