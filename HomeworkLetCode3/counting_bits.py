"""Given an integer n, return an array ans of length n + 1 such that
    for each i (0 <= i <= n), ans[i] is the number of 1's in the binary representation of i.
    I used "https://stackoverflow.com/questions/9829578/
            fast-way-of-counting-non-zero-bits-in-positive-integer"
"""

def count_bits(n_num: int) -> list[int]:
    """"Main function"""
    def helper_count(h_num: int) -> int:
        """Helper function"""
        h_num = (h_num & 0x5555555555555555) + ((h_num & 0xAAAAAAAAAAAAAAAA) >> 1)
        h_num = (h_num & 0x3333333333333333) + ((h_num & 0xCCCCCCCCCCCCCCCC) >> 2)
        h_num = (h_num & 0x0F0F0F0F0F0F0F0F) + ((h_num & 0xF0F0F0F0F0F0F0F0) >> 4)
        h_num = (h_num & 0x00FF00FF00FF00FF) + ((h_num & 0xFF00FF00FF00FF00) >> 8)
        h_num = (h_num & 0x0000FFFF0000FFFF) + ((h_num & 0xFFFF0000FFFF0000) >> 16)
        h_num = (h_num & 0x00000000FFFFFFFF) + ((h_num & 0xFFFFFFFF00000000) >> 32)
        return h_num
    return [helper_count(i) for i in range(n_num+1)]

if __name__ == "__main__":
    print(count_bits(5))
