"""A phrase is a palindrome if, after converting all uppercase letters into lowercase
    letters and removing all non-alphanumeric characters, it reads the same forward and backward.
    Alphanumeric characters include letters and numbers.
    Given a string s, return true if it is a palindrome, or false otherwise.
"""
import re

def is_palindrome(s_in: str) -> bool:
    """Check String Palindrome"""
    s_str: str = re.sub('[^A-Za-z0-9]+', '', s_in).lower()
    s_leng: int = len(s_str)
    for i_c in range(s_leng//2):
        if s_str[i_c] != s_str[s_leng - i_c - 1]:
            return False
    return True

if __name__ == "__main__":
    tests = ["A man, a plan, a canal: Panama",
             "race a car",
             " "]

    for i in tests:
        print(f"test {i} -> {is_palindrome(i)}")
