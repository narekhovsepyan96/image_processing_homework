"""Write a function that reverses a string. The input string is given as an array of characters s.
    You must do this by modifying the input array in-place with O(1) extra memory.
"""



def reverse_string(s_str: list[str]) -> list[str]:
    """Do not return anything, modify s in-place instead."""
    r_len = len(s_str)
    for i_r in range(r_len//2):
        s_str[i_r],s_str[r_len-i_r-1] = s_str[r_len-i_r-1],s_str[i_r]
    return s_str

if __name__ == "__main__":
    tests = [["h","e","l","l","o"],
             ["H","a","n","n","a","h"]]
    for i in tests:
        print(f"tests {i} -> {reverse_string(i)}")
