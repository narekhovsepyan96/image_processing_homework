"""
 Given a non-empty array of integers nums, every element appears twice except for one.
 Find that single one.
 You must implement a solution with a linear runtime complexity and use only constant extra space.
"""


def single_number(in_nums: list[int]) -> int:
    """Bitwise Xor number find single Number"""
    xor_x: int = 0
    for i_c in in_nums:
        xor_x ^= i_c
    return xor_x



if __name__ == "__main__" :
    n_tests = [[2,2,1],
             [1,5,6,7,8,7,1,6,8],
             [4,1,2,1,2],
             [6,100,78,5,6,78,5]]

    for i in n_tests:
        print(f"test {i} -> {single_number(i)}")
