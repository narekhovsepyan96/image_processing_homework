/*Write a program that calculates Nth Fibonacci number and prints it to console. N should be passed to the program trough the std::cin.
  The goal is to understand what is the highest value you can calculate(find max N and its result)
  bonus: optimized code (the same value is not calculated more than once)*/

#include<iostream>
#include<vector>
#include<string>
#include<sstream>
using ull = unsigned long long;

int main(){
	ull N{},i{2};
	ull a{1},b{},c{};
	std::string input;
	std::cout << "Insert 'q' to exit program " << std::endl;
	while(1){
		std::cin >> input;

		if(input == "q")
			return{};
		if(std::cin.fail())
			std::cout << "Enter Int" << std::endl;

		N = std::strtoull(input.c_str(),nullptr,10);

		for(;i!=N+1;++i){
			c = a + b;
			if(a+b < a)
				break;
			std::cout << i << "->" << c << std::endl;
			b = a;
			a = c;
		}
	}
	return{};
}
