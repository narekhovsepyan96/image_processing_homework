#include<iostream>
#include<limits>




int main(){
	
	std::cout << "  	max and min value\n";
	std::cout << "int    -> " << std::showpos <<  std::numeric_limits<int>::max() << " "
			  << std::numeric_limits<int>::min() << std::endl
			  << std::scientific << "float  -> " << std::numeric_limits<float>::max() << " "
			  << std::numeric_limits<float>::lowest() <<std::endl
			  << "double -> " << std::numeric_limits<double>::max() << " "
			  << std::numeric_limits<double>::lowest() <<std::endl
			  << "char   -> "  << (int)std::numeric_limits<char>::max() << " "
			  << (int)std::numeric_limits<char>::min() << std::endl << " " 
			  << "bool   -> " << std::boolalpha << std::numeric_limits<bool>::max()  << " "
			  << std::numeric_limits<bool>::min() << std::endl;
	std::cout << "\n" << std::endl;

	std::cout << std::noshowpos << "unsigned int -> " << std::numeric_limits<unsigned int>::max() << " "
			  << std::numeric_limits<unsigned int>::min() << std::endl
			  << "unsigned char -> " << (int)std::numeric_limits<unsigned char>::max() << " "
			  << (int)std::numeric_limits<unsigned char>::min() << std::endl;
	
	std::cout << "cast" << std::endl;
	std::cout << "double to short -> " << static_cast<short>(std::numeric_limits<double>::max()) << " "
			  << static_cast<short>(std::numeric_limits<double>::min()) << std::endl
			  << "short to double -> " << static_cast<double>(std::numeric_limits<short>::max()) << " "
			  << static_cast<short>(std::numeric_limits<short>::min()) << std::endl
			  << "double to int" << static_cast<int>(std::numeric_limits<double>::max()) << " "
			  << static_cast<int>(std::numeric_limits<double>::lowest()) << std::endl
			  << "int to double -> " << static_cast<double>(std::numeric_limits<int>::max()) << " "
			  << static_cast<double>(std::numeric_limits<int>::min()) << std::endl
			  << "char to int -> " << static_cast<int>(std::numeric_limits<char>::max()) << " "
			  << static_cast<int>(std::numeric_limits<char>::min()) << std::endl
			  << "int to char -> " << static_cast<char>(std::numeric_limits<int>::max()) << " "
			  << static_cast<char>(std::numeric_limits<char>::min()) << std::endl
			  << "float to long long -> " << static_cast<long long>(std::numeric_limits<float>::max()) << " "
			  << static_cast<long long>(std::numeric_limits<float>::lowest()) << std::endl
			  << "long long to float -> " << static_cast<float>(std::numeric_limits<long long>::max()) << " "
			  << static_cast<float>(std::numeric_limits<long long>::min()) << std::endl
			  << "double to float -> " << static_cast<float>(std::numeric_limits<double>::max()) << " "
			  << static_cast<float>(std::numeric_limits<double>::lowest()) << std::endl << " "
			  << "float to double -> " << static_cast<double>(std::numeric_limits<float>::max()) << " "
			  << static_cast<double>(std::numeric_limits<float>::lowest()) << std::endl;
	return{};
}
