#include<iostream>


int main(){
	
	std::cout << "Hello\tWorld!" << std::endl;
	// tab size World
	std::cout << "Hello\aWorld!" << std::endl;
	// audio signal printing
	std::cout << "Hello\bWorld!" << std::endl;
	// delete left letter in word
	std::cout << "Hello\fWorld!" << std::endl;
	// mew line vertically down
	std::cout << "Hello\nWorld!" << std::endl;
	// new line
	std::cout << "Hello\rWorld!" << std::endl;
	// new line operand right string
	std::cout << "Hello\vWorld!" << std::endl;
	// \v equivalent \f new line vertically down
	std::cout << "Hello\135World!" << std::endl;
	// insert symbol octal presentation
	std::cout << "Hello\x5dWorld!" << std::endl;
	// inser symbol hexadecimall presentation
	std::cout << "Hello\U0001F525World!" << std::endl;
	std::cout << "Hello\U0001F526World!" << std::endl;
	// \c color image in the form of a symbol
	std::cout << "\U0001F32A" << std::endl;
	// \Unnn color image in the form of a symbol
	std::cout << "\u058D" << std::endl;
	// \unnn image in the form a 
	return{};
}
