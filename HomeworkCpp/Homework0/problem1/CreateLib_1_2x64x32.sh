echo g++ -c -m32 lib1.cpp >> *x32.txt
g++ -c -m32 lib1.cpp -o lib1x32.o >> *x32.txt
size lib1x32.o >> *x32.txt

echo >> *x32.txt
echo g++ -c -m64 lib1.cpp >> *x32.txt
g++ -c -m64 lib1.cpp -o lib1x64.o *x32.txt
size lib1x64.o >> *x32.txt

echo >> *x32.txt
echo g++ -c -m32 lib2.cpp >> *x32.txt
g++ -c -m32 lib2.cpp -o lib2x32.o >> *x32.txt
size lib2x32.o >> *x32.txt
echo >> *x32.txt

echo g++ -c -m64 lib2.cpp >> *x32.txt
g++ -c -m64 lib2.cpp -o lib2x64.o *x32.txt
size lib2x64.o >> *x32.txt

echo >> *x32.txt
echo "Create Lib lib1 and lib2 x32 x64" >> *x32.txt
echo >> *x32.txt


ar crs lib1x32.a lib1x32.o
echo ar crs lib1x32.a lib1x32.o >> *x32.txt
size lib1x32.a >> *x32.txt

ar crs lib1x64.a lib1x64.o
echo ar crs lib1x64.a lib1x64.o >> *x32.txt
size lib1x64.a >> *x32.txt


ar crs lib2x32.a lib2x32.o
echo ar crs lib2x32.a lib2x32.o >> *x32.txt
size lib2x32.a >> *x32.txt

ar crs lib2x64.a lib2x64.o
echo ar crs lib2x64.a lib2x64.o >> *x32.txt  
size lib2x64.a >> *x32.txt






