#################### X64 ################

g++ -c -O0 -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    308	      8	      1	    317	    13d	lib2.o
g++ -c -O1 -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    215	      8	      1	    224	     e0	lib2.o
g++ -c -O2 -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    213	      8	      1	    222	     de	lib2.o
g++ -c -O3 -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    213	      8	      1	    222	     de	lib2.o
g++ -c -Os -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    196	      8	      1	    205	     cd	lib2.o
g++ -c -Og -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    277	      8	      1	    286	    11e	lib2.o
g++ -c -Ofast -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    213	      8	      1	    222	     de	lib2.o
################## X32 ###############

g++ -c -O0 -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    414	      4	      1	    419	    1a3	lib2.o
g++ -c -O1 -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    306	      4	      1	    311	    137	lib2.o
g++ -c -O2 -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    306	      4	      1	    311	    137	lib2.o
g++ -c -O3 -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    306	      4	      1	    311	    137	lib2.o
g++ -c -Os -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    290	      4	      1	    295	    127	lib2.o
g++ -c -Og -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    385	      4	      1	    390	    186	lib2.o
g++ -c -Ofast -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    306	      4	      1	    311	    137	lib2.o
################ clang -Oz Optimization ################

clang -c -Oz -m64 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    144	      8	      1	    153	     99	lib2.o
clang -c -Oz -m32 lib2.cpp
   text	   data	    bss	    dec	    hex	filename
    194	      4	      1	    199	     c7	lib2.o
