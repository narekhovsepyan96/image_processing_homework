PKGS="-O0 -O1 -O2 -O3 -Os -Og -Ofast"
AR="-m64 -m32"
pt=true

echo "#################### X64 ################" >> doclib2.txt
echo >> doclib2.txt
for m in $AR
do
  for p in $PKGS
   do
   echo "g++ -c $p $m lib2.cpp" >> doclib2.txt
   g++ -c $p $m lib2.cpp
   size lib2.o >> doclib2.txt
   done
   if $pt; then 
   	echo "################## X32 ###############" >> doclib2.txt
   	echo >> doclib2.txt
	pt=false
   fi
done

echo "################ clang -Oz Optimization ################" >> doclib2.txt
echo >> doclib2.txt
echo "clang -c -Oz -m64 lib2.cpp" >> doclib2.txt
clang -c -Oz -m64 lib2.cpp
size lib2.o >> doclib2.txt
echo "clang -c -Oz -m32 lib2.cpp" >> doclib2.txt
clang -c -Oz -m32 lib2.cpp
size lib2.o >> doclib2.txt
