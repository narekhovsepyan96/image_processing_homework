PKGS="-O0 -O1 -O2 -O3 -Os -Og -Ofast"                                                                                                                            
AR="-m64 -m32"


echo "#################### X64 ################" >> DocMain_x64x32.txt
echo >> DocMain_x64x32.txt


for i in $PKGS
do
		echo "g++ -m32 $i main.cpp lib2x32.a lib1x32.a -o main32.out" >> DocMain_x64x32.txt
		g++ -m32 $i main.cpp lib2x32.a lib1x32.a -o main32.out
		size main32.out >> DocMain_x64x32.txt
		echo >> DocMain_x64x32.txt
done

echo >> DocMain_x64x32.txt
echo "#################### X64 ################" >> DocMain_x64x32.txt
echo >> DocMain_x64x32.txt

for i in $PKGS
do
        echo "g++ -m64 $i main.cpp lib2x64.a lib1x64.a -o main64.out" >> DocMain_x64x32.txt
        g++ -m64 $i main.cpp lib2x64.a lib1x64.a -o main64.out
        size main64.out >> DocMain_x64x32.txt
        echo >> DocMain_x64x32.txt
done


			
