#include<iostream>
#include<chrono>



int main(){
	int **matrix1 = new int*[256]{};
	int **matrix2 = new int*[256]{};
	int **matrix3 = new int*[256]{};
	for(unsigned i{};i!=256;++i){
		matrix1[i] = new int[256]{};
		matrix2[i] = new int[256]{};
		matrix3[i] = new int[256]{};
	}
	int tmp{};
	for(unsigned i{};i!=256;++i){
		for(unsigned j{};j!=256;++j){
			tmp = std::rand() % 1000;
			matrix1[i][j] = tmp;
			matrix2[j][i] = tmp;
		}
	}



	auto start = std::chrono::steady_clock::now();	
	for(unsigned i{};i!=256;++i){
		for(unsigned j{};j!=256;++j){
			for(unsigned k{};k!=256;++k){
				matrix3[i][j] += matrix1[j][k] + matrix2[k][j];
			}
		}
	}
	auto end = std::chrono::steady_clock::now();

	std::chrono::duration<double> sec = end - start;

	std::cout << "convetional method -> " << sec.count() << "\n\n"; 


	for(unsigned i{255};i!=-1;--i){
		delete[] matrix3[i];
		delete[] matrix2[i];
		delete[] matrix1[i];

	}
	delete[] matrix3;
	delete[] matrix2;
	delete[] matrix1;
	return{};
}
