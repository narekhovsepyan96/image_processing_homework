#include<iostream>
#include<chrono>








int main(){
	const unsigned n = 256;
	int tmp{};
	int** matrix1 = new int*[n]{};
	int** matrix2 = new int*[n]{};
	int** matrix3 = new int*[n]{};

	for(unsigned i{};i!=n;++i){
		for(unsigned j{};j!=n;++j){
			matrix3[i] = new int[n]{};
			matrix2[i] = new int[n]{};
			matrix1[i] = new int[n]{};
		}
	}

	for(unsigned i{};i!=n;++i){
		for(unsigned j{};j!=n;++j)
		{
			tmp = std::rand() % 1000;
			matrix1[i][j] = tmp;
			matrix2[j][i] = tmp;
		}
	}

	tmp = 0;
	auto start = std::chrono::steady_clock::now();
	for(unsigned i{};i!=n;++i){
		for(unsigned j{};j!=n;++j){
			tmp = 0;
			for(unsigned k{};k!=n;++k){
				tmp += matrix1[i][k] * matrix2[k][j];
			}
			matrix3[i][j] = tmp;
		}
	}
	auto end = std::chrono::steady_clock::now();

	std::chrono::duration<double> sec = end - start;

	std::cout << "ijk & jik -> " << sec.count() << "\n\n";


	for(unsigned i{n-1};i!=-1;--i){
			delete[] matrix3[i];
			delete[] matrix2[i];
			delete[] matrix1[i];
	}
	delete[] matrix3;
	delete[] matrix2;
	delete[] matrix1;
	
	return{};
}
