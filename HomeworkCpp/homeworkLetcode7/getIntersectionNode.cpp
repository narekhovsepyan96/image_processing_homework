#include<iostream>

struct ListNode{
	int val;
	ListNode* next;
	ListNode(const int& item): val{item},next{nullptr} { }
};


void destroy(ListNode* in){
	ListNode* tmp{nullptr};
	while(in){
		tmp = in;
		in = in->next;
		delete tmp;
	}
}

ListNode* getIntersectionNode(ListNode* headA, ListNode* headB){
	ListNode * tmp = headB;
	while(headA){
		while(headB){
			if(headA == headB)
				return headA;
			headB = headB->next;
		}
		headB = tmp;
		headA = headA->next;
	}
	return nullptr;
}



ListNode* getIntersectionNode(ListNode* headA,ListNode* headB,ListNode* posB){
	if(!posB){
		posB = headB;
		if(headA)
			headA = headA->next;
		else return nullptr;
	}
	if(headA == posB)
		return headA;
	return getIntersectionNode(headA,headB,posB->next);
}

ListNode* Intersection(ListNode* headA, ListNode* headB){
	if(!headB->next)
		std::swap(headA,headB);
	return getIntersectionNode(headA,headB,headB);
}
int main(){
	ListNode* headA = new ListNode{4};
	headA->next = new ListNode{1};
	headA->next->next = new ListNode{8};
	headA->next->next->next = new ListNode{4};
	headA->next->next->next->next = new ListNode{5};

	ListNode* headB = new ListNode{5};
	headB->next = new ListNode{6};
	headB->next->next = new ListNode{1};
	headB->next->next->next = headA->next->next;
	
	ListNode* tmp0 = headA;
	ListNode* tmp1 = headB;

	ListNode* out =  getIntersectionNode(tmp0, tmp1);
	std::cout << out->val << std::endl;

	tmp0 = headA;
	tmp1 = headB;
	out = Intersection(tmp0,tmp1);
	std::cout << out->val << std::endl;

	headB->next->next->next = nullptr;
	destroy(headA);
	destroy(headB);
	return{};
}




