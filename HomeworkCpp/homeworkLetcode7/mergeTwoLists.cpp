#include<iostream>


struct ListNode{
	int val;
	ListNode* next;
	ListNode(const int& item): val{item},next{nullptr} { }
};


void destroy(ListNode* in){
	ListNode* tmp{nullptr};
	while(in){
		tmp = in;
		in = in->next;
		delete tmp;
	}
}

ListNode* iterative_mergeTwoLists(ListNode*& list1,ListNode*& list2){
	ListNode* out{nullptr};
	ListNode* pos{nullptr};
	while(list1 && list2){
		if(list1->val < list2->val){
			if(!out){
				out = list1;
				pos = out;
				list1 = list1->next; 
			}
			else{
				pos->next = list1;
				list1 = list1->next;
				pos = pos->next;
			}
	  }
	  else{
		if(!out){
			out = list2;
			pos = out;
			list2 = list2->next;
		}
		else{
			pos->next = list2;
			list2 = list2->next;
			pos = pos->next;
		}
	 }

	}
	while(list1)
		pos->next =list1,
		list1 = list1->next,
		pos = pos->next;

	while(list2)
		pos->next = list2,
		list2 = list2->next,
		pos = pos->next;

	return out;
}

void recursive_mergeTwoLists(ListNode*& list1,ListNode*& list2,ListNode*& pos){
	if(!list1 && !list2)
		return;
	else if(list1 && list2){
		if(list1->val < list2->val){
			pos->next = list1;
			list1 = list1->next;
		}
		else{
			pos->next = list2;
			list2 = list2->next;
		}
	}
	else if(list1){
		pos->next = list1;
		list1 = list1->next;
	}
	else if(list2){
		pos->next = list2;
		list2 = list2->next;
	}
	recursive_mergeTwoLists(list1,list2,pos->next);
}


ListNode* input_recursive_mergeTwoLists(ListNode* list1,ListNode* list2){
	if(!list1 && list2)
		return list2;
	else if(list1 && !list2)
		return list1;
	else if(!list1 && !list2)
		return nullptr;

	ListNode* out{nullptr};
	ListNode* root{nullptr};
	if(list1->val <= list2->val){
		out = list1;
		root = out;
		list1 = list1->next;
		recursive_mergeTwoLists(list1,list2,out);
		return root;
	}
	out = list2;
	root = out;
	list2 = list2->next;
	recursive_mergeTwoLists(list1,list2,out);
	return root;
}





int main(){

	ListNode* list1 = new ListNode{1};
	list1->next = new ListNode{2};
	list1->next->next = new ListNode{4};

	ListNode* list2 = new ListNode{1};
	list2->next = new ListNode{3};
	list2->next->next = new ListNode{4};
	ListNode* out{nullptr};
	//out = iterative_mergeTwoLists(list1,list2); 
	out = input_recursive_mergeTwoLists(list1,list2);
	ListNode* tmp = out;
	while(tmp){
		std::cout << tmp->val << " ";
		tmp = tmp->next;
	}
	list1 = nullptr;
	list2 = nullptr;
	destroy(out);
	return{};
}














