#include<iostream>
#include<vector>


struct node{
	int val;
	node* next;
	node(const int& item):val{item},next{nullptr} { }
};

node* root{nullptr};
node* head{nullptr};

void del_nodes(){
	node* tmp{nullptr};
	while(head){
		tmp = head->next;
		delete head;
		head = tmp;
	}
}


void insert(const int& item){
	if(!head){
		root = new node{item};
		head = root;
		return;
	}

	root->next = new node{item};
	root = root->next;
}


void Iterative_deleteDublicates(node*& head){
	node* tmp{nullptr};
	node* root = head;
	node* tmp0{head};
	root = root->next;
	
	while(root){
		if(tmp0->val == root->val){
			if(root->next){
				tmp = root;
				root = root->next;
				delete tmp;
				tmp0->next = root;
			}
			else{
				delete root;
				root = nullptr;
				tmp0->next = nullptr;
			}
		}
		else{
			tmp0 = root;
			root = root->next;
		}
	}
}

void Recursive_deleteDublicates(node* in,node* pos){
	if(!pos)
		return;

	if(in->val == pos->val){
		if(pos->next){
			node* tmp = pos->next;
			delete pos;
			pos = tmp;
			in->next = pos;
		}
		else{
			delete pos;
			pos = nullptr;
			in->next = nullptr;
		}
	}
	else{
		in = pos;
	}
	Recursive_deleteDublicates(in,in->next);
}

int main(){
	std::vector<int> v{1,1,1,2,2,2,3,3,3,5,5,5,6,7,7,7};
	for(const auto& i:v){
		insert(i);
	}
	node* tmp = head;
	//Iterative_deleteDublicates(tmp);
	Recursive_deleteDublicates(head,head->next);
	while(tmp){
		std::cout << tmp->val << " ";
		tmp = tmp->next;
	}
	return {};
}




















