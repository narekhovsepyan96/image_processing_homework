#include<iostream>
#include<vector>

struct node{
	int val;
	node* next;
	node(const int& item):val{item},next{nullptr}{ }
};


void destroy(node* head){
	node* tmp{nullptr};
	while(head){
		tmp = head;
	 	head = head->next;
		delete tmp;
	}
}


node* iterative_012partition(node* head){
	node* curr{head};
	node* next{head};
	for(int i{1};i!=3;++i){
		while(curr){
			if(curr->val < i){
				int l = curr->val;
				curr->val = next->val;
				next->val = l;
				if(next && next->next)
					next = next->next;
			}
			curr = curr->next;
		}
		curr = head;
		next = head;
	}
	return head;
}

node* recursive_012partition(node*& head,node*& curr,node*& next,int i = 1){
	if(curr){
		if(curr->val < i){
			int l = next->val;
			next->val = curr->val;
			curr->val = l;

			if(next && next->next)
				next = next->next;
		}
		curr = curr->next;
	}
	else if(i == 3){
		return head;
	}
	else if(!curr){
		++i;
		curr = head;
		next = head;
	}

	return recursive_012partition(head,curr,next,i);
}



int main(){
	
	std::vector<int> v{ 0,1,2,2,1,0,1,2,2,1,0,0,1,2,1,2,2,0,0 };
	node* head{nullptr};
	node* pos{nullptr};

	for(const auto& i:v){
		if(!head){
			head = new node{i};
			pos = head;
			continue;
		}
	    pos->next = new node{i};
		pos = pos->next;

	}
	node* tmp; //= iterative_012partition(head);
	node* curr = head;
	node* next = head;
	tmp = recursive_012partition(head,curr,next);
	while(tmp){
		std::cout << tmp->val << " ";
		tmp = tmp->next;
	}
	

	
	destroy(head);
	return {};
}

