#include<iostream>

struct node{
	int val;
	node* next;
	node(const int& item) : val{item},next{nullptr} { }
};

node* root{nullptr};
node* root1{nullptr};
void insert(const int& val){
	if(!root){
		root = new node{val};
		root1 = root;
	}
	else{
		root->next = new node{val};
		root = root->next;
	}
}

void del_node(){
	node* tmp{nullptr};
	while(root1){
		tmp = root1;
		root1 = root1->next;
		delete tmp;
	}
}

void reverse_iterative(node** in){
	node* root{nullptr};
	node* tmp{nullptr};
	
	while(*in){
		tmp = *in;
		*in = (*in)->next;
		tmp->next = root;
		root = tmp;
	}
	*in = &*root;
	root1 = root;
}

// tail no optimize
node* reverse_tail_recursive_nopt(node* in){
	if(!in || !in->next)
		return in;
	node* tmp = reverse_tail_recursive_nopt(in->next);
	in->next->next = in;
	in->next = nullptr;
	return tmp;
}

// tail optimize
void reverse_tail_recursive_opt(node* curr,node* prev, node** head){
	if(!curr->next){
		*head = curr;
		curr->next = prev;
		return;
	}
	node* next = curr->next;
	curr->next = prev;
	reverse_tail_recursive_opt(next,curr,head);
}

void reverse_tail_input(node** in){
	reverse_tail_recursive_opt(*in,nullptr,in);
}


int main(){
	
	for(int i{};i!=6;++i){
		insert(i);
	}
	node* tmp0{nullptr};
	//reverse_iterative(&root1);
	//tmp0 = reverse_tail_recursive_nopt(root1);
	//root1 = tmp0;
	reverse_tail_input(&root1);
	tmp0 = root1;
	while(tmp0){
		std::cout << tmp0->val << " ";
		tmp0 = tmp0->next;
	}
	


	del_node();
	return{};
}





