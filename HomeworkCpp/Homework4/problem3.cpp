#include<iostream>


void Pswap(int& a, int& b){
	a = a + b;
	b = a - b;
	a = a - b;
}

void Xswap(int& a,int& b){
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
}


int main(){
	int a = 5;
	int b = 7;
	std::cout << "a = " << a << " b = " << b << std::endl;
	Xswap(a,b);
	std::cout << "a = " << a << " b = " << b << std::endl;
	Pswap(a,b);
	std::cout << "a = " << a << " b = " << b << std::endl;
	return 0;
}


