#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include<algorithm>
#include<iterator>
bool str(const std::string& in){
	int s{-1};
	for(const auto& i: in){
		if(i != '.')
			if(i!= '_')
				if(i!='@')
					if(std::isalnum(i) == 0)
						return false;
		if(i == '@')
			++s;
	}
	if(s == -1)
		return false;
	if(s > 0)
		return false;

	return true;
}


bool name(const std::string& in){
	int s = in.find('@');
	
	if(s == 0)
		return false;
	for(int i{};i < s;++i){
		if(in[i]!= '.')
			if(in[i]!='_')
				if(std::isalnum(in[i]) == 0)
					return false;
		
	}
	return true;
		
}

bool webpage(const std::string& in){
	unsigned s{},k{};

	s = in.find('@');
	if(s > in.size())
		return false;
	k = in.find_last_of('.');
	if(k > in.size())
		return false;
	
	if(s - k == 0)
		return false;

	for(unsigned i = s+1;i < k;++i){
		if(std::isalnum(in[i]) == 0)
			return false;
	}
	
	return true;
}


bool domain(const std::string& in){
	int k{};
	
	k = in.find_last_of('.');
	if(k > in.size())
		return false;

	if(in.size() - k - 1 == 0)
		return false;

	for(unsigned i = k + 1;i < in.size();++i){
		if(std::isalpha(in[i]) == 0)
			return false;
	}
	return true;
}

bool is_valid(const std::string& in){
	if(str(in)){
		if(name(in)){
			if(webpage(in)){
				if(domain(in)){
					return true;
				}
			}
		}
	}
	
	return false;
}


int main(int argc, char** argv){
	std::fstream fs(*(++argv), std::ios::in | std::ios::out);

	std::for_each(std::istream_iterator<std::string>{fs},
				 std::istream_iterator<std::string>{},
				 [&](auto in){
						if(!is_valid(in))
							std::cout << std::boolalpha <<  in << " " << std::endl;
				 });
	std::cout << "\nis not valid\n";

	return {};
}




