#include<iostream>
#include<bit>
#include<limits>

template<typename T>
T rotr(T x, int s);
template<typename T>
T rotl(T x, int s);


template<typename T>
T rotr(T x, int s){

	decltype(std::numeric_limits<T>::digits) N = std::numeric_limits<T>::digits;
	decltype(s % N) r = s % N;

	return r > 0 ? (x >> r) | (x << (N - r)) : rotl(x,-r);
}

template<typename T>
T rotl(T x,int s){
	decltype(std::numeric_limits<T>::digits) N = std::numeric_limits<T>::digits;

	decltype(s % N) r = s % N;

	return r > 0 ? (x << r | (x >> (N - r))) : rotr(x,-r);

}

enum class Direction{
	left = 0,
	right = 1
};

template<typename T>
bool shift_bits(T number, int shift, Direction d, int bit_index){
	decltype(number) res = 1;
	res <<= bit_index;

	if(d == Direction::left)
		return rotl(number,shift) & res;
	return rotr(number,shift) & res;
}


int main(){

	std::uint8_t a = 0b00101110;
	
	std::cout << std::boolalpha <<  shift_bits(a,1,Direction::left,4) << "\n";



	return {};
}

