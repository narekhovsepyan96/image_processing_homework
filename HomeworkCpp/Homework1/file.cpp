#include<iostream>
#include<string>
#include<fstream>

bool my_isalpha(unsigned char ch){
	unsigned tmp = static_cast<unsigned>(ch);
	if((64 < tmp && tmp < 91) || (96 < tmp && tmp < 123))
		return true;
	return false;
}


int main(int argc,char** argv){;	
	std::fstream fs(*(++argv),std::ios::in | std::ios::out);
	std::string tmp;
	int pos{};
	while(std::getline(fs,tmp)){
		pos = tmp.size()+1;
		if(my_isalpha(tmp[0]))
			tmp[0] = std::toupper(tmp[0]);
		
		for(unsigned i{1};i!=pos-1;++i){
			if(my_isalpha(tmp[i]) && !my_isalpha(tmp[i+1])){
				tmp[i] = std::toupper(tmp[i]);
			}
			if(!my_isalpha(tmp[i]) && my_isalpha(tmp[i+1])){
				tmp[i+1] = std::toupper(tmp[i+1]);
			}
		}
		fs.seekp(-pos,std::ios::cur);
		fs << tmp + "\n";
		tmp.clear();

	}

	fs.close();
	return{};
}
