#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include<algorithm>

int main(int argc,char** argv){
	std::fstream fs(*(++argv),std::ios::in | std::ios::out);
	std::vector<char> tmp;
	unsigned pos{};
	std::copy(std::istreambuf_iterator<char>{fs},
			  std::istreambuf_iterator<char>{},
			  std::back_inserter(tmp));
	pos = tmp.size();
	if(std::isalpha(tmp[0]))
		tmp[0] = std::toupper(tmp[0]);
	if(std::isalpha(tmp[pos-1]))
		tmp[pos-1] = std::toupper(tmp[pos-1]);

	for(unsigned i{1};i!=pos-1;++i){
		if(std::isalpha(tmp[i]) && !std::isalpha(tmp[i+1]))
			tmp[i] = std::toupper(tmp[i]);
		if(!std::isalpha(tmp[i]) && std::isalpha(tmp[i+1]))
			tmp[i+1] = std::toupper(tmp[i+1]);
	}

	fs.seekp(fs.ios_base::beg);
	std::copy(tmp.begin(),tmp.end(),
			  std::ostreambuf_iterator<char>{fs});



	fs.close();
	return {};
}
