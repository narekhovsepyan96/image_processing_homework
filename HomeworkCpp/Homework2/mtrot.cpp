#include<iostream>
#include<vector>


int main(){
	std::vector<std::vector<int>> v{{1,2,3},
									{4,5,6},
									{7,8,9}};
	
	unsigned len = v.size();
	for(unsigned i{};i!=len;++i){
		for(unsigned j{},k= len-1;j !=  k;++j,--k){
			std::swap(v[j][i],v[k][i]);
		}
	}
	for(unsigned i{};i!=len;++i){
		for(unsigned j = i;j!=len;++j){
			std::swap(v[j][i],v[i][j]);

		}
	}



	for(const auto& i:v){
		for(const auto& j:i)
			std::cout << j << " ";
		std::cout << std::endl;
	}


	return{};
}
