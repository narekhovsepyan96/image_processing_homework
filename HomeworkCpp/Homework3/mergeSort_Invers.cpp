#include<iostream>
#include<vector>
#include<fstream>

void merge(std::vector<int>& v,std::vector<int>& tmp,const unsigned& left,
		  const unsigned& mid, const unsigned& right,unsigned* inv = nullptr){
	unsigned lpos{left}, rpos{mid+1};
	for(unsigned i{left};i!=right+1;++i)
		tmp[i] = v[i];

	for(unsigned i{left};i <= right;++i)
		if(lpos > mid)
			v[i] = tmp[rpos++];
		else if(rpos > right)
			v[i] = tmp[lpos++];
		else if(tmp[lpos] <= tmp[rpos])
			v[i] = tmp[lpos++];
		else {
			v[i] = tmp[rpos++];
			if(inv)
				*inv = *inv + (mid -lpos)+1;
		}
}

void Rmerge_sort(std::vector<int>& v,std::vector<int>& tmp,const unsigned& left,
		         const unsigned& right,unsigned* inv = nullptr){
	unsigned mid = left + (right - left)/2;
	if(left < right)
		Rmerge_sort(v,tmp,left,mid,inv),
		Rmerge_sort(v,tmp,mid+1,right,inv),
		::merge(v,tmp,left,mid,right,inv);
}

unsigned R_inv_merge_sort(std::vector<int>& v){
	unsigned inv{};
	std::vector<int> tmp(v.size());
	Rmerge_sort(v,tmp,0,v.size()-1,&inv);
	return inv;
}

unsigned I_inv_merge_sort(std::vector<int>& v){
	unsigned inv{};
	unsigned n = v.size();
	std::vector<int> tmp(n);
	for(unsigned i{1}; i < n; i = i + i)
		for(unsigned j{};j < n - i;j += i+i)
			merge(v,tmp,j,j+i-1,std::min(j+i+i-1,n-1),&inv);
	return inv;
}



int main(int argc, char** argv){
	std::fstream fs(*(++argv),std::ios::in | std::ios::out);
	std::vector<int> to_vector;
	std::copy(std::istreambuf_iterator<char>{fs},
			  std::istreambuf_iterator<char>{},
			  std::back_inserter(to_vector));
	std::vector<int> t1(to_vector);
	std::cout << "Recursive Meerge Sort Inversion -> " << R_inv_merge_sort(to_vector) << std::endl;
	
	std::cout << "Iterative Merge Sort Inversion -> " << I_inv_merge_sort(t1) << std::endl;
	
	fs.close();
	return{};
}











