#!/bin/bash
PFILE="Prandom.txt"
NFILE="Nrandom.txt"
Pfor()
{
	for i in {0..100000}
	do 
		echo $RANDOM >> $PFILE
	done
}

Nfor()
{
	for i in {0..100000}
	do
		echo $((RANDOM % 200000 - 100000)) >> $NFILE
	done
}



if [ -f "$PFILE" ] && [ -f "$NFILE" ]; then
	truncate -s 0 $PFILE
	truncate -s 0 $NFILE
	Pfor
	Nfor
else 
	touch $NFILE
	touch $PFILE
	Pfor
	Nfor
fi

g++ mergeSort_Invers.cpp -o mergeSort_Invers

echo "Positive"
./mergeSort_Invers $PFILE
echo "Negative"
./mergeSort_Invers $NFILE





